/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evasao;

import java.sql.SQLException;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Natália Amaral
 */
@ManagedBean
public class controleSituacao {

    Situacao situacao = new Situacao();
    Db db = new Db();

    public List<Situacao> getlistaSitucao() throws SQLException {
        List<Situacao> situacoes = situacao.getlistaSituacao();

        return situacoes;

    }
}
