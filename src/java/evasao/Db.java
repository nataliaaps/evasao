package evasao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static javafx.beans.binding.Bindings.not;
import static javax.management.Query.not;
import javax.xml.registry.ConnectionFactory;

public class Db {

    private String url = "jdbc:postgresql://200.18.128.54/db_evasao";
    private String usuario = "evasao";
    private String senha = "evaso";
    private Connection con;
    private Statement stm;
    private String driver = "org.postgresql.Driver";
    int idAulaLecionada = 0;

    ResultSet rs = null;
    Situacao situacao = new Situacao();
    List<Situacao> listaSituacao = new ArrayList<>();

    //
    private Connection minhaConexao;

    public Db() {
//        this.minhaConexao = new Connection;
    }

    public List<Situacao> situacaoAluno() throws SQLException {

        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, usuario, senha);
            stm = con.createStatement();
            String sql = "SELECT nome, situacao_curso.situacao_curso\n"
                    + "	FROM public.aluno\n"
                    + "	INNER JOIN situacao_curso on aluno.id_situacao_curso = situacao_curso.id_situacao_curso";

            stm.executeQuery(sql);
            rs = stm.getResultSet();
            while (rs.next()) {
                situacao.setNome(rs.getString("nome"));
                situacao.setSituacao(rs.getString("situacao"));
                listaSituacao.add(situacao);
            }

        } catch (Exception e) {

            System.out.println("Erro selecionar id aulas lecionadas: " + e);
        }

        con.close();
        return listaSituacao;

    }

}
